//
//  JSONUtils.m
//  20211027-YingzhuoLi-NYCSchools
//
//  Created by Yingzhuo Li on 10/27/21.
//

#import "JSONUtils.h"
#import "School.h"
#import "SchoolSAT.h"

@implementation JSONUtils
// JSON endpoints for schools data
static NSString *NYC_SCHOOLS = @"https://data.cityofnewyork.us/resource/s3k6-pzi2.json";
static NSString *NYC_SCHOOLS_SAT = @"https://data.cityofnewyork.us/resource/f9bf-2cp4.json";

// Get NSDictionary from json string
+ (NSArray *) arrayWithJSON:(NSString *)json {
    NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
    
    if (!jsonData) {
        return nil;
    }
    
    NSError *error = nil;
    NSArray *list =
    [NSJSONSerialization JSONObjectWithData:jsonData
                                    options:NSJSONReadingMutableContainers
                                      error:&error];
    if (!list) {
        NSLog(@"%@", error);
        list = nil;
    }
    
    return list;
}

// Get data from URL using HTTP GET
+ (void) requestDataWithURL:(NSString *)url
               andCallback:(void (^)(NSURLResponse *response, NSData *data,
                                     NSError *error))callback {
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] init];
    [req setURL:[NSURL URLWithString:url]];
    [req setTimeoutInterval:10.0];
    [req setHTTPMethod:@"GET"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSLog(@"Sending request to URL: '%@'", url);
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithRequest:req
                completionHandler:^(NSData *data, NSURLResponse *response,
                                    NSError *error) {
        if (callback != nil) {
            callback(response, data, error);
        }
    }] resume];
}

#pragma mark - Web service RPC methods

+ (void) getSchools:(void (^)(NSArray *list,
                              NSError *error))callback {
    [JSONUtils requestDataWithURL:NYC_SCHOOLS andCallback:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (error != nil) {
            callback(nil, error);
        } else {
            NSString *json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSArray *list = [JSONUtils arrayWithJSON:json];
            if ([list isKindOfClass:[NSArray class]]) {
                list = [School arrayWithDictionaryArray:list];
                callback(list, nil);
            } else {
                callback(nil, nil);
            }
        }
    }];
}

+ (void) getSchoolSATs:(void (^)(NSDictionary *list,
                              NSError *error))callback {
    [JSONUtils requestDataWithURL:NYC_SCHOOLS_SAT andCallback:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (error != nil) {
            callback(nil, error);
        } else {
            NSString *json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSArray *list = [JSONUtils arrayWithJSON:json];
            if ([list isKindOfClass:[NSArray class]]) {
                NSDictionary *dict = [SchoolSAT mapWithDictionaryArray:list];
                callback(dict, nil);
            } else {
                callback(nil, nil);
            }
        }
    }];
}

@end
