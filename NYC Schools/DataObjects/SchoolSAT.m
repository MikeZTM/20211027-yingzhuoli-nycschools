//
//  SchoolSAT.m
//  20211027-YingzhuoLi-NYCSchools
//
//  Created by Yingzhuo Li on 10/26/21.
//

#import "SchoolSAT.h"

@implementation SchoolSAT

- (NSString *) getDBN {
    return _dictionary[@"dbn"];
}

- (NSString *) getReadingScore {
    return _dictionary[@"sat_critical_reading_avg_score"];
}
- (NSString *) getMathScore {
    return _dictionary[@"sat_math_avg_score"];
}
- (NSString *) getWritingScore {
    return _dictionary[@"sat_writing_avg_score"];
}

+ (NSArray<SchoolSAT *> *) arrayWithDictionaryArray:(NSArray *)list {
    NSMutableArray *res = [NSMutableArray array];
    for (NSDictionary *item in list) {
        SchoolSAT *school = [[SchoolSAT alloc] init];
        school.dictionary = item;
        [res addObject:school];
    }
    return [res copy];
}

+ (NSDictionary *) mapWithDictionaryArray:(NSArray *)list {
    NSMutableDictionary *res = [NSMutableDictionary dictionary];
    for (NSDictionary *item in list) {
        SchoolSAT *school = [[SchoolSAT alloc] init];
        school.dictionary = item;
        [res setObject:school forKey:school.getDBN];
    }
    return [res copy];
}

@end
