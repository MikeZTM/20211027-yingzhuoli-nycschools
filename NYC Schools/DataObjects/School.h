//
//  School.h
//  20211027-YingzhuoLi-NYCSchools
//
//  Created by Yingzhuo Li on 10/27/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface School : NSObject
@property NSDictionary *dictionary;

- (NSString *) getName;
- (NSString *) getDBN;
- (NSString *) getPhoneNumber;
- (NSString *) getLocation;
- (double) getLatitude;
- (double) getLongitude;

+ (NSArray<School *> *) arrayWithDictionaryArray:(NSArray *)list;
@end

NS_ASSUME_NONNULL_END
