//
//  SchoolSAT.h
//  20211027-YingzhuoLi-NYCSchools
//
//  Created by Yingzhuo Li on 10/26/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SchoolSAT : NSObject
@property NSDictionary *dictionary;

- (NSString *) getDBN;
- (NSString *) getReadingScore;
- (NSString *) getMathScore;
- (NSString *) getWritingScore;

+ (NSArray<SchoolSAT *> *) arrayWithDictionaryArray:(NSArray *)list;
+ (NSDictionary *) mapWithDictionaryArray:(NSArray *)list;
@end

NS_ASSUME_NONNULL_END
