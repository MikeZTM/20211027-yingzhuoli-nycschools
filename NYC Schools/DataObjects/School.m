//
//  School.m
//  20211027-YingzhuoLi-NYCSchools
//
//  Created by Yingzhuo Li on 10/27/21.
//

#import "School.h"

@implementation School

- (NSString *) getName {
    return _dictionary[@"school_name"];
}

- (NSString *) getDBN {
    return _dictionary[@"dbn"];
}

- (NSString *) getPhoneNumber {
    return _dictionary[@"phone_number"];
}

- (NSString *) getLocation {
    return [NSString stringWithFormat:@"%@, %@ %@",
            _dictionary[@"primary_address_line_1"],
            _dictionary[@"city"], _dictionary[@"zip"]];
}

- (double) getLatitude {
    return ((NSString *)_dictionary[@"latitude"]).doubleValue;
}

- (double) getLongitude {
    return ((NSString *)_dictionary[@"longitude"]).doubleValue;
}

+ (NSArray<School *> *) arrayWithDictionaryArray:(NSArray *)list {
    NSMutableArray *res = [NSMutableArray array];
    for (NSDictionary *item in list) {
        School *school = [[School alloc] init];
        school.dictionary = item;
        [res addObject:school];
    }
    return [res copy];
}

@end
