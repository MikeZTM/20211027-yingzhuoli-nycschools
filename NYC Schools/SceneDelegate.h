//
//  SceneDelegate.h
//  NYC Schools
//
//  Created by Mike Lee on 10/26/21.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

