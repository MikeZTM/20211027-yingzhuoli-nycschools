//
//  ViewController.m
//  NYC Schools
//
//  Created by Yingzhuo Li on 10/26/21.
//

#import "SchoolDetailViewController.h"
#import "School.h"
#import "SchoolSAT.h"

@interface SchoolDetailViewController ()

@end

@implementation SchoolDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.title = [_school getName];
    self.addressLabel.text = [_school getLocation];
    
    if (_schoolSAT != nil) {
        self.mathLabel.text = [NSString stringWithFormat:@"Math Score: %@", [_schoolSAT getMathScore]];
        self.readingLabel.text = [NSString stringWithFormat:@"Reading Score: %@", [_schoolSAT getReadingScore]];
        self.writingLabel.text = [NSString stringWithFormat:@"Writing Score: %@", [_schoolSAT getWritingScore]];
    } else {
        self.mathLabel.text = @"No SAT score find for this school";
    }
    
    MKPointAnnotation *marker = [[MKPointAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake([_school getLatitude], [_school getLongitude]) title:[_school getName] subtitle:nil];
    [_mapView addAnnotation:marker];
    MKCoordinateSpan span = MKCoordinateSpanMake(0.002, 0.002);
    MKCoordinateRegion region = MKCoordinateRegionMake(marker.coordinate, span);
    [_mapView setRegion:region];
    
}


@end
