//
//  ViewController.h
//  NYC Schools
//
//  Created by Yingzhuo Li on 10/26/21.
//

#import <UIKit/UIKit.h>
@import MapKit;
@class School;
@class SchoolSAT;

@interface SchoolDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *mathLabel;
@property (weak, nonatomic) IBOutlet UILabel *readingLabel;
@property (weak, nonatomic) IBOutlet UILabel *writingLabel;

@property School *school;
@property SchoolSAT *schoolSAT;

@end

