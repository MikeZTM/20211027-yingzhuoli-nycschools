//
//  SchoolsTableViewController.m
//  20211027-YingzhuoLi-NYCSchools
//
//  Created by Yingzhuo Li on 10/26/21.
//

#import "SchoolsTableViewController.h"
#import "JSONUtils.h"
#import "School.h"
#import "SchoolSAT.h"
#import "SchoolDetailViewController.h"

@interface SchoolsTableViewController ()

@end

@implementation SchoolsTableViewController {
    NSArray<School *> *schools;
    NSDictionary *schoolSATs;
    int fetched;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    fetched = 0;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self fetchData];
}

- (void)showError:(NSError * _Nonnull)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.presentedViewController != nil && [self.presentedViewController isKindOfClass:[UIAlertController class]]) {    //already showing alert popup
            return;
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:nil preferredStyle:UIAlertControllerStyleAlert];
        if (error != nil) {
            alert.message = [NSString stringWithFormat:@"Can not fetch school data: %@", error.description];
        } else {
            alert.message = @"Can not fetch school data";
        }
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
            [self fetchData];   //retry button
        }];

        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    });
}

- (void) fetchData {
    [JSONUtils getSchools:^(NSArray * _Nonnull list, NSError * _Nonnull error) {    //fetch schools list
        if (list.count > 0 && [list.firstObject isKindOfClass:[School class]]) {
            self -> schools = list;
            self -> fetched++;
            if (self -> fetched == 2) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableView reloadData];
                });
            }
        } else {
            [self showError:error];
        }
    }];
    [JSONUtils getSchoolSATs:^(NSDictionary * _Nonnull dict, NSError * _Nonnull error) {    //fetch SAT scores
        if (dict.allKeys.count > 0 && [dict[dict.allKeys.firstObject] isKindOfClass:[SchoolSAT class]]) {
            self -> schoolSATs = dict;
            self -> fetched++;
            if (self -> fetched == 2) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableView reloadData];
                });
            }
        } else {
            [self showError:error];
        }
    }];
}

// Looking for SAT object by school DBN
- (SchoolSAT *)getSATFromSchool:(School *)school {
    if (school == nil) {
        return nil;
    }
    NSString *dbn = [school getDBN];
    SchoolSAT *sat = [schoolSATs.allKeys containsObject:dbn] ? schoolSATs[dbn] : nil;
    return sat;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return schools.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"idSchoolCell" forIndexPath:indexPath];
    
    cell.textLabel.text = [[schools objectAtIndex:indexPath.row] getName];
    
    return cell;
}

// Click on table cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"segShowDetailVC" sender:[schools objectAtIndex:indexPath.row]];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"segShowDetailVC"]) {
        if ([sender isKindOfClass:[School class]]
            && [segue.destinationViewController isKindOfClass:[SchoolDetailViewController class]]) {
            SchoolSAT *sat = [self getSATFromSchool:sender];
            
            SchoolDetailViewController *destVC = segue.destinationViewController;
            destVC.school = sender;
            destVC.schoolSAT = sat;
        }
    }
}

@end
