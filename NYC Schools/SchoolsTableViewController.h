//
//  SchoolsTableViewController.h
//  20211027-YingzhuoLi-NYCSchools
//
//  Created by Yingzhuo Li on 10/26/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SchoolsTableViewController : UITableViewController

@end

NS_ASSUME_NONNULL_END
