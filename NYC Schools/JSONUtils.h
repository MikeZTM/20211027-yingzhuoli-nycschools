//
//  JSONUtils.h
//  20211027-YingzhuoLi-NYCSchools
//
//  Created by Mike Lee on 10/26/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface JSONUtils : NSObject
+ (void) getSchools:(void (^)(NSArray *list,
                              NSError *error))callback;
+ (void) getSchoolSATs:(void (^)(NSDictionary *list,
                                 NSError *error))callback;
@end

NS_ASSUME_NONNULL_END
