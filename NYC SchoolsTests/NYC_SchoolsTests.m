//
//  NYC_SchoolsTests.m
//  NYC SchoolsTests
//
//  Created by Yingzhuo Li on 10/26/21.
//

#import <XCTest/XCTest.h>
#import "JSONUtils.h"
#import "School.h"
#import "SchoolSAT.h"

@interface NYC_SchoolsTests : XCTestCase

@end

@implementation NYC_SchoolsTests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testGetSchools {
    XCTestExpectation *getSchoolsExpectation = [self expectationWithDescription:@"Get schools list"];
    [JSONUtils getSchools:^(NSArray * _Nonnull list, NSError * _Nonnull error) {
        if (list.count > 0 && [list.firstObject isKindOfClass:[School class]]) {
            XCTAssert(true);
            [getSchoolsExpectation fulfill];
        } else {
            XCTAssert(false);
            [getSchoolsExpectation fulfill];
        }
    }];
    // Timeout
    [self waitForExpectationsWithTimeout:10 handler:^(NSError *error) {
        if (error != nil) {
            XCTAssert(false);
        }
    }];
}

- (void)testGetSchoolSATs {
    XCTestExpectation *getSchoolsExpectation = [self expectationWithDescription:@"Get school SAT list"];
    [JSONUtils getSchoolSATs:^(NSDictionary * _Nonnull dict, NSError * _Nonnull error) {
        if (dict.allKeys.count > 0 && [dict[dict.allKeys.firstObject] isKindOfClass:[SchoolSAT class]]) {
            XCTAssert(true);
            [getSchoolsExpectation fulfill];
        } else {
            XCTAssert(false);
            [getSchoolsExpectation fulfill];
        }
    }];
    // Timeout
    [self waitForExpectationsWithTimeout:10 handler:^(NSError *error) {
        if (error != nil) {
            XCTAssert(false);
        }
    }];
}

//- (void)testPerformanceExample {
//    // This is an example of a performance test case.
//    [self measureBlock:^{
//        // Put the code you want to measure the time of here.
//    }];
//}

@end
